/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.shapeproject;

/**
 *
 * @author ACER
 */
public class Square {
    private double s;
    
    public Square (double s) {
        this.s = s;
    }
    
    public double calArea(){
        return s * s;
    }
    
    public double getS() {
        return this.s = s;
    }
    
    public void setS(double s) {
        if(s <= 0) {
            System.out.println("ERROR!!!");
            return;
        }
        this.s = s;
    }
}

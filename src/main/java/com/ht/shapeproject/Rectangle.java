/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.shapeproject;

/**
 *
 * @author ACER
 */
public class Rectangle {
    private double w;
    private double h;
    
    public Rectangle (double w,double h) {
        this.w = w;
        this.h = h;
    }
    
    public double calArea() {
        return w*h;
    }
    
    public double getW() {
        return this.w;
    }
    
    public double getH() {
        return this.h;
    }
    
    public void SetRect (double w , double h) {
        if(w <= 0 || h <= 0) {
            System.out.println("ERROR!!!");
            return;  
        }
        this.w = w;
        this.h = h;
    }
    
}

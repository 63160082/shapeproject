/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.shapeproject;

/**
 *
 * @author ACER
 */
public class Triangle {
    private double b;
    private double h;
    
    public Triangle (double b , double h) {
        this.b = b;
        this.h = h;
    }
    
    public double calArea() {
        return this.b * this.h / 2;
    }
    
    public double getB() {
        return this.b;
    }
    
    public double getH() {
        return this.h;
    }
    
    public void SetTri (double b , double h) {
        if(b <= 0 || h <= 0) {
            System.out.println("ERROR!!!");
            return;
        }
        this.b = b;
        this.h = h;
    }
}
